import Vue from 'vue'

import LoadingIcon from '~/components/common/loading-icon'
Vue.component('LoadingIcon', LoadingIcon)

import CardDeviceInfo from '~/components/card-device-info'
Vue.component('CardDeviceInfo', CardDeviceInfo)

